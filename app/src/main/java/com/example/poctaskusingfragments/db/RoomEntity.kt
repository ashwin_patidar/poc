package com.example.poctaskusingfragments.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "userinfo")
data class RoomEntity(
    @ColumnInfo(name="albumId")
    val albumId: Int,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    val id: Int =0,
    @ColumnInfo(name="title")
    val title: String,
    @ColumnInfo(name="url")
    val url: String,
    @ColumnInfo(name = "thumbnailUrl")
    val thumbnailUrl: String
)
