package com.example.poctaskusingfragments.db

import androidx.room.*

@Dao
interface RoomDao {

    @Query("SELECT * FROM userinfo ORDER BY id DESC")
    fun getAllUserInfo(): List<RoomEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: RoomEntity?)

    @Delete
    suspend fun deleteUser(user: RoomEntity?)

    @Update
    suspend fun updateUser(user: RoomEntity?)

    @Query("SELECT EXISTS(SELECT * FROM userinfo WHERE id =:id)")
    fun isRecordExistsUserInfo(id: Int) : Boolean
}