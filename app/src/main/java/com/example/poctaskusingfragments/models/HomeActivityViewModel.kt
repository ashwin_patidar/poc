package com.example.poctaskusingfragments.models

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.poctaskusingfragments.db.RoomDB
import com.example.poctaskusingfragments.db.RoomEntity

class HomeActivityViewModel(app: Application): AndroidViewModel(app) {
    lateinit var allUsers : MutableLiveData<List<RoomEntity>>

    init{
        allUsers = MutableLiveData()
    }

    fun getAllUsersObservers(): MutableLiveData<List<RoomEntity>>{
        return allUsers
    }

    suspend fun getAllUsers(){
        val userDao = RoomDB.getAppDatabase(getApplication())?.userDao()
        val list = userDao?.getAllUserInfo()
        allUsers.postValue(list!!)
    }

    suspend fun insertUserInfo(entity: RoomEntity){
        val userDao = RoomDB.getAppDatabase(getApplication())?.userDao()
        val list = userDao?.insertUser(entity)
        getAllUsers()
    }

    suspend fun deleteUserInfo(entity: RoomEntity){
        val userDao = RoomDB.getAppDatabase(getApplication())?.userDao()
        val list = userDao?.deleteUser(entity)
        getAllUsers()
    }

    suspend fun updateUserInfo(entity: RoomEntity){
        val userDao = RoomDB.getAppDatabase(getApplication())?.userDao()
        val list = userDao?.updateUser(entity)
        getAllUsers()
    }

}


