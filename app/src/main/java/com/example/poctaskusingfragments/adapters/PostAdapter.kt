package com.example.poctaskusingfragments.adapters

import android.app.Fragment
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.findFragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
//import com.example.poctaskusingfragments.fragments.HomeFragmentDirections
import com.example.poctaskusingfragments.R
import com.example.poctaskusingfragments.databinding.FragmentHomeBinding
//import com.example.poctaskusingfragments.databinding.FragmentDetailBinding
//import com.example.poctaskusingfragments.fragments.FragmentHomeBinding
import com.example.poctaskusingfragments.db.RoomDB
import com.example.poctaskusingfragments.db.RoomEntity
import com.example.poctaskusingfragments.fragments.DetailFragment
import com.example.poctaskusingfragments.fragments.HomeFragment
import com.example.poctaskusingfragments.fragments.HomeFragmentDirections
import com.example.poctaskusingfragments.models.HomeActivityViewModel
import com.example.poctaskusingfragments.models.PhotoModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostAdapter(val context: Context, val photoModel: MutableList<PhotoModel>, val mainViewModel: HomeActivityViewModel): RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    var binding: PostAdapter? = null
//    lateinit var viewModel: HomeActivityViewModel

    var count2 = 0
    var items = ArrayList<RoomEntity>()

    fun setListData(data: ArrayList<RoomEntity>) {
        this.items = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        return holder.bindView(photoModel[position])
    }

    override fun getItemCount(): Int {
        return photoModel.size
    }

    var count: Int = 0


    inner class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvTitle = itemView.findViewById<TextView>(R.id.title)
        private val thumbImage: ImageView = itemView.findViewById<ImageView>(R.id.image)
        private val btn = itemView.findViewById<ConstraintLayout>(R.id.frag)
        // private val btn: Button = itemView.findViewById<ImageButton>(R.id.btn)


        fun bindView(photoModel: PhotoModel) {

                tvTitle.text = photoModel.title

                thumbImage.load(photoModel?.thumbnailUrl)

                btn.setOnClickListener {
                    val fv = arrayOf(
                        photoModel.albumId.toString(),
                        photoModel.id.toString(),
                        photoModel.title,
                        photoModel.url,
                        photoModel.thumbnailUrl
                    )
                    val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(fv)
                    Navigation.findNavController(it).navigate(action)
                }

                itemView.findViewById<ImageButton>(R.id.imgbtn).setOnClickListener {
                    val c1 = itemView.findViewById<ImageButton>(R.id.imgbtn)
                    if (c1.isPressed) {
                        c1.setImageResource(R.drawable.ic_baseline_favorite_24)
                        val user = RoomEntity(
                            photoModel.albumId!!,
                            photoModel.id!!,
                            photoModel.title!!,
                            photoModel.url!!,
                            photoModel.thumbnailUrl!!
                        )
                        CoroutineScope(Dispatchers.IO).launch {

                            mainViewModel.insertUserInfo(user)
                            //
                        }
                        Log.e("Deletion", "was success")
                    } else {
                      //  itemView.findViewById<ImageView>(R.id.imageview)
                            c1.setImageResource(R.drawable.ic_baseline_add_box_24)
                        val user = RoomEntity(
                            photoModel.albumId!!,
                            photoModel.id!!,
                            photoModel.title!!,
                            photoModel.url!!,
                            photoModel.thumbnailUrl!!
                        )

                        CoroutineScope(Dispatchers.IO).launch {

                            mainViewModel.deleteUserInfo(user)
                            //
                        }
                        Log.e("INserted", "was success")

                    }
                }
            }
        }


    }
//}

//}
