package com.example.poctaskusingfragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.poctaskusingfragments.R
import com.example.poctaskusingfragments.db.RoomEntity
import com.example.poctaskusingfragments.models.HomeActivityViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class FavouriteAdapter(val viewModel: HomeActivityViewModel): RecyclerView.Adapter<FavouriteAdapter.PostViewHolder>() {

    var items = ArrayList<RoomEntity>()

    fun setListData(data: ArrayList<RoomEntity>) {
        this.items = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.favourite_row, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        return holder.bindView(items[position])
    }

    override fun getItemCount(): Int {
      return items.size
    }

    // var post: MutableList<PhotoModel> ?= null
    inner class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvTitle = itemView.findViewById<TextView>(R.id.title)
        private val thumbImage: ImageView = itemView.findViewById<ImageView>(R.id.image)
        fun bindView(photoModel: RoomEntity) {
            tvTitle.text = photoModel.title
            thumbImage.load(photoModel?.thumbnailUrl)

            val user = RoomEntity(
                photoModel.albumId!!,
                photoModel.id!!,
                photoModel.title!!,
                photoModel.url!!,
                photoModel.thumbnailUrl!!
            )
            itemView.findViewById<ImageButton>(R.id.imgbtn).setOnClickListener {
                CoroutineScope(Dispatchers.IO).launch {
                    viewModel.deleteUserInfo(user)
//                    notifyDataSetChanged()
                }
            }
        }


    }
}