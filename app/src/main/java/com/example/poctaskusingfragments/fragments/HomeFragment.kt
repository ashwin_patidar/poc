package com.example.poctaskusingfragments.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.poctaskusingfragments.adapters.FavouriteAdapter
import com.example.poctaskusingfragments.adapters.PostAdapter
import com.example.poctaskusingfragments.api.ApiService
import com.example.poctaskusingfragments.api.ServiceGenerator
import com.example.poctaskusingfragments.databinding.FragmentHomeBinding
import com.example.poctaskusingfragments.models.HomeActivityViewModel
import com.example.poctaskusingfragments.models.PhotoModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeFragment : Fragment(){

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var binding: FragmentHomeBinding
        binding = FragmentHomeBinding.inflate(inflater, container, false)
//        val view = binding?.root

        lateinit var recyclerViewAdapter: FavouriteAdapter
        val serviceGenerator = ServiceGenerator.buildService(ApiService::class.java)
        val call = serviceGenerator.getPosts()


        val rv= binding?.recycler

        var mainViewModel: HomeActivityViewModel

        call.enqueue(object : Callback<MutableList<PhotoModel>> {
            @SuppressLint("FragmentLiveDataObserve")
            override fun onResponse(
                call: Call<MutableList<PhotoModel>>,
                response: Response<MutableList<PhotoModel>>
            ) {

                if (response.isSuccessful) {

                    mainViewModel = ViewModelProviders.of(this@HomeFragment).get(
                        HomeActivityViewModel::class.java)
//
//                   mainViewModel.getAllUsersObservers().observe(this@HomeFragment, Observer {
//                     recyclerViewAdapter.notifyDataSetChanged()
//                    })

    rv?.layoutManager = LinearLayoutManager(context)
    rv?.adapter = context?.let { PostAdapter(context = it, response.body()!!, mainViewModel) }
    rv?.setHasFixedSize(true)

//                    binding?.recyclerView?.layoutMode = LinearLayoutManager(this@HomeFragment)
//                    binding?.recyclerView?.adapter = PostAdapter(response.body()!!)
//                    binding?.recyclerView?.setHasFixedSize(true)
                   Log.e("Insertion","to Room Success!!!")

                }
            }

                override fun onFailure(call: Call<MutableList<PhotoModel>>, t: Throwable) {
                    t.printStackTrace()
                    Log.e("Error",t.message.toString())
                }
        })
            return binding?.root
        }

//    fun  getHomeModel(){
//        var viewModel= ViewModelProviders.of(this).get(HomeActivityViewModel::class.java)
//        viewModel.getAllUsersObservers().observe( this,{
//        })
//    }
}