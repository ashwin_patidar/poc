package com.example.poctaskusingfragments.fragments

import android.app.ActionBar
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.poctaskusingfragments.adapters.FavouriteAdapter
import com.example.poctaskusingfragments.databinding.FragmentFavouriteBinding
import com.example.poctaskusingfragments.db.RoomEntity
import com.example.poctaskusingfragments.models.HomeActivityViewModel
import com.example.poctaskusingfragments.models.PhotoModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class FavouriteFragment : Fragment() {
    lateinit var viewModel: HomeActivityViewModel

    lateinit var recyclerViewAdapter: FavouriteAdapter
    var favourites: MutableList<PhotoModel> ?= null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding : FragmentFavouriteBinding
        // Inflate the layout for this fragment
        binding = FragmentFavouriteBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this).get(HomeActivityViewModel::class.java)

        val rv= binding?.recycle
        rv?.layoutManager = LinearLayoutManager(context)

        CoroutineScope(Dispatchers.IO).launch {
//            viewModel.insertUserInfo(RoomEntity(1, 2, "natus nisi omnis corporis facere molestiae rerum in", "sssss", "https://via.placeholder.com/150/f66b97"))
            viewModel.insertUserInfo(RoomEntity(1, 2, "gggggg", "sssss", "sssss"))
          viewModel.deleteUserInfo(RoomEntity(1, 2, "gggggg", "sssss", "sssss"))
        }
            //   recyclerViewAdapter.notifyDataSetChanged()
//        rv?.adapter = recyclerViewAdapter
            viewModel.getAllUsersObservers().observe(viewLifecycleOwner, Observer {
                recyclerViewAdapter.setListData(ArrayList(it))
                recyclerViewAdapter.notifyDataSetChanged()
            })
      //  }
        recyclerViewAdapter = FavouriteAdapter(viewModel)

        rv?.adapter = recyclerViewAdapter

     //   CoroutineScope(Dispatchers.IO).launch {
//                Log.e("data",ArrayList(viewModel.getAllUsersObservers().value).toString()!!)
        // }

        Log.e("Favo","fetched success!!!")
      //  val list = viewModel.allUsers
        return binding?.root
    }

}