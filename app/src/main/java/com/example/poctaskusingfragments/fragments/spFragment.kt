package com.example.poctaskusingfragments.fragments

import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.poctaskusingfragments.R
import com.example.poctaskusingfragments.databinding.FragmentSpBinding


class spFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        var binding: FragmentSpBinding
        binding = FragmentSpBinding.inflate(inflater, container, false)

        val timer = object: CountDownTimer(4000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }
            override fun onFinish() {
               Navigation.findNavController(binding?.root).navigate(R.id.action_spFragment_to_fragmentA)
            }
        }
        timer.start()

        return  binding?.root
    }
}