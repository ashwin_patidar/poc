package com.example.poctaskusingfragments.fragments

import android.app.ActionBar
import android.app.PendingIntent.getActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import coil.api.load
import com.example.poctaskusingfragments.R
import com.example.poctaskusingfragments.databinding.FragmentDetailBinding
import com.example.poctaskusingfragments.models.HomeActivityViewModel
import com.google.android.material.internal.ContextUtils.getActivity
import kotlin.jvm.internal.Ref


class DetailFragment : Fragment() {

  //  val args: DetailFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
      val args: DetailFragmentArgs by navArgs()
        var binding: FragmentDetailBinding

        val viewModel: HomeActivityViewModel

        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater, container, false)

          viewModel = ViewModelProviders.of(this).get(HomeActivityViewModel::class.java)

        binding?.title?.text = args.favourites[2]
        binding?.image?.load(args.favourites[4])

setMenuVisibility(true)
    setHasOptionsMenu(true)
    //    activity?.actionBar?.setBackgroundDrawable(resources.getDrawable(R.drawable.abc_vector_test))

        return binding?.root
    }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    inflater.inflate(R.menu.menuu, menu)
    return super.onCreateOptionsMenu(menu, inflater)
  }
}