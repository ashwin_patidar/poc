package com.example.poctaskusingfragments.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.poctaskusingfragments.R
import com.example.poctaskusingfragments.databinding.FragmentABinding
import com.example.poctaskusingfragments.databinding.FragmentDetailBinding


class fragmentA : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding: FragmentABinding
        // Inflate the layout for this fragment
        binding = FragmentABinding.inflate(inflater, container, false)

        binding?.home?.setOnClickListener {
            Navigation.findNavController(binding?.root).navigate(R.id.action_fragmentA_to_homeFragment)
        }

        binding?.favourite?.setOnClickListener {
            Navigation.findNavController(binding?.root).navigate(R.id.action_fragmentA_to_favouriteFragment)
        }

        return binding?.root!!
    }
}


