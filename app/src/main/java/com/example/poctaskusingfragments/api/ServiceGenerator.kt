package com.example.poctaskusingfragments.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ServiceGenerator {
    // building a client to fetch the Data from Api using Retrofit!!!...
    private val client = OkHttpClient.Builder().build()

    // Building the Retrofit Network Connection

    private val retrofit = Retrofit.Builder().
                           baseUrl("https://jsonplaceholder.typicode.com").
                           addConverterFactory(GsonConverterFactory.create()).
                           client(client).build()

    fun <T> buildService(service: Class<T>): T {
        return retrofit.create(service)
    }
}