package com.example.poctaskusingfragments.api
import com.example.poctaskusingfragments.models.PhotoModel
import retrofit2.Call
import retrofit2.http.GET


interface ApiService {

    @GET("/photos")
    fun getPosts(): Call<MutableList<PhotoModel>>
}